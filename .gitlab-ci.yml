---
workflow:
  rules:
    - if: '$CI_PROJECT_PATH == "gitlab-org/terraform-images"'
      when: never
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_REF_PROTECTED == 'true'

variables:
  BASE_IMAGE: "alpine:3.18.4"
  TERRAFORM_IMAGE_NAME: "$CI_REGISTRY_IMAGE/branches/$CI_COMMIT_REF_SLUG-$TERRAFORM_VERSION:$CI_COMMIT_SHA"
  DOCKER_DIND_IMAGE: "docker:24.0.7-dind"
  PLATFORMS: linux/amd64,linux/arm64
  STABLE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/stable:latest"
  STABLE_VERSION: "1.5"

.terraform-versions:
  parallel:
    matrix:
      - TERRAFORM_BINARY_VERSION: "1.5.7"
        TERRAFORM_VERSION: "1.5"
      # - TERRAFORM_BINARY_VERSION: "1.4.6"
      #   TERRAFORM_VERSION: "1.4"
      # - TERRAFORM_BINARY_VERSION: "1.3.9"
      #   TERRAFORM_VERSION: "1.3"
      # - TERRAFORM_BINARY_VERSION: "1.2.9"
      #   TERRAFORM_VERSION: "1.2"
      # - TERRAFORM_BINARY_VERSION: "1.1.9"
      #   TERRAFORM_VERSION: "1.1"

stages:
  - test
  - build
  - deploy

shell check:
  stage: test
  image: koalaman/shellcheck-alpine:v0.9.0
  before_script:
    - shellcheck --version
  script:
    - shellcheck src/**/*.sh

dockerfile check:
  stage: test
  image: hadolint/hadolint:latest-alpine
  before_script:
    - hadolint --version
  script:
    - hadolint --ignore DL3059 --ignore DL3006 --ignore DL3018 Dockerfile

build terraform:
  extends: .terraform-versions
  stage: build
  services:
    - "$DOCKER_DIND_IMAGE"
  image: "$DOCKER_DIND_IMAGE"
  before_script:
    # See https://docs.docker.com/build/building/multi-platform/#qemu
    # To be able to build for arm64 which is not native to hosted runners,
    # we need to inject a stub for being able to do so.
    #
    # The container below is used by the docs article above and hence
    # considered upstream, even though it's in a non-Docker namespace.
    - docker run --rm --privileged tonistiigi/binfmt
    # Registry auth
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker buildx create --use
    # NOTE: we disable provenance for now
    # because it causes troubles with the registry and older clients.
    # See
    # - https://gitlab.com/gitlab-org/terraform-images/-/issues/104
    # - https://gitlab.com/gitlab-org/terraform-images/-/merge_requests/184#note_1328485943
    - docker buildx build
      --platform "$PLATFORMS"
      --build-arg BASE_IMAGE=$BASE_IMAGE
      --build-arg TERRAFORM_BINARY_VERSION=$TERRAFORM_BINARY_VERSION
      --file Dockerfile
      --tag "$TERRAFORM_IMAGE_NAME"
      --provenance=false
      --push
      .

release-terraform:
  extends: .terraform-versions
  stage: deploy
  variables:
    RELEASE_IMAGE_NAME: "$CI_REGISTRY_IMAGE/releases/$TERRAFORM_VERSION"
  image:
    name: gcr.io/go-containerregistry/crane:debug
    entrypoint: [""]
  script:
    # https://github.com/google/go-containerregistry/blob/main/cmd/crane/doc/crane_copy.md
    - crane auth login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
    - crane copy "$TERRAFORM_IMAGE_NAME" "$RELEASE_IMAGE_NAME:latest"
    - crane copy "$TERRAFORM_IMAGE_NAME" "$CI_REGISTRY_IMAGE/releases/terraform:$TERRAFORM_BINARY_VERSION"
    - crane copy "$TERRAFORM_IMAGE_NAME" "$RELEASE_IMAGE_NAME:$CI_COMMIT_TAG"
    - if [ "$TERRAFORM_VERSION" = "$STABLE_VERSION" ]; then crane copy "$TERRAFORM_IMAGE_NAME" "$STABLE_IMAGE_NAME"; fi
  rules:
    - if: $CI_COMMIT_TAG

release:
  stage: deploy
  needs: [release-terraform]
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script: echo "Creating release $CI_COMMIT_TAG"
  release:
    tag_name: $CI_COMMIT_TAG
    description: "terraform-images release $CI_COMMIT_TAG"
